# == Class: xnat::groovy
#
# Helper class to install Groovy from a downloaded binary zip.
#
# === Parameters
#
# Below are parameters expected by this class. Required vs. optional parameters are noted.
#
# [*binary_zip_url*]
#   URL for Groovy binary zip.
#   Default: https://bintray.com/artifact/download/groovy/maven/apache-groovy-binary-2.4.7.zip
#
# [*binary_zip_md5*]
#   Optional, MD5 checksum to verify download of Groovy binary zip.
#   Default: undef (aka no checksumming)
#
# [*groovy_version*]
#   The version of Groovy in the binary zip specified above.
#   Default: 2.4.7
#
# [*groovy_home*]
#   The path under which to unpack the Groovy binary zip, and to set as GROOVY_HOME in the machine's
#   environment variables.  Note that ${GROOVY_HOME}/bin will also be appended to the system path.
#   Default: /opt/groovy
#
# === Variables
#
# Custom variables used by this class.
#
# NONE
#
# === Examples
#
#  class { 'xnat::groovy':
#    binary_zip_url => 'https://bintray.com/groovy/maven/download_file?file_path=apache-groovy-binary-2.4.4.zip',
#    groovy_version => '2.4.4',
#  }
#
# === Authors
#
# NRG <webmaster@nrg.wustl.edu>
#
# === Copyright
#
# Copyright 2016 NRG.
#
class xnat::groovy (
  String $binary_zip_url = 'https://bintray.com/artifact/download/groovy/maven/apache-groovy-binary-2.4.7.zip',
  String $groovy_version = '2.4.7',
  String $groovy_home = '/opt/groovy',
  Optional[String] $binary_zip_md5 = undef,
  ){

  include wget
  include java

  $fetch_dest = "/opt/puppet_xnat_groovy/"
  $zip_filename = basename($binary_zip_url)
  $groovy_dirname = basename($binary_zip_url, '.zip')
  $groovy_home_real = "${groovy_home}/groovy-${groovy_version}"

  ensure_packages([ 'unzip' ], {'ensure' => 'latest'})

  common::mkdir_p { $fetch_dest: }
  ->
  common::mkdir_p { $groovy_home: }
  ->
  wget::fetch { 'wget_groovy_binary_zip':
    source => $binary_zip_url,
    destination => $fetch_dest,
  }
  ->
  exec {
    'unzip_groovy_binary_zip':
      command => "unzip -qo ${fetch_dest}${zip_filename}",
      path    => ['/usr/bin', '/usr/sbin', '/bin'],
      cwd => $groovy_home,
      creates => $groovy_home_real,
      require           => [ Package['unzip'], Class['java'] ];
  }

  # Set GROOVY_HOME environment and update PATH
  file {
    "/etc/profile.d/groovy_home.sh":
      owner             => root,
      group             => root,
      mode              => "0755",
      replace           => true,
      content           => template("xnat/groovy_home.sh.erb"),
      require           => Class['java'];
    "/etc/profile.d/groovy_home.csh":
      owner             => root,
      group             => root,
      mode              => "0755",
      replace           => true,
      content           => template("xnat/groovy_home.csh.erb"),
      require           => Class['java'];
  }

}
