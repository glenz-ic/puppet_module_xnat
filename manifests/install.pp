# == Class: xnat::install
#
# Private class for XNAT installation
#
class xnat::install {

  include xnat

  # Chose the install class based on method and version specified
  case $::xnat::version {
    '1.6': {
      case $::xnat::install_method {
        "source": {
          # Retrieve XNAT artifacts, declare dependencies
          class{ 'xnat::install::source_xnat16': }
          File[$::xnat::data_root] -> Class['xnat::install::source_xnat16']
          File[$::xnat::pipeline_path_real] -> Class['xnat::install::source_xnat16']
        }
        default: {
          fail("XNAT puppet module support for installation method ${::xnat::install_method} and version ${::xnat::version} is unsupported.")
        }
      }
    }
    '1.7': {
       case $::xnat::install_method {
        "WAR": {
          # Retrieve XNAT artifacts, declare dependencies
          class{ 'xnat::install::war_xnat17': }
          File[$::xnat::xnat_home_real] -> Class['xnat::install::war_xnat17']
          File[$::xnat::pipeline_path_real] -> Class['xnat::install::war_xnat17']
        }
        default: {
          fail("XNAT puppet module support for installation method ${::xnat::install_method} and version ${::xnat::version} is unsupported.")
        }
      }     
    }
  }
}
