# == Class: xnat::tomcat::tomcat_xnat16
#
# Private class to handle Tomcat config for XNAT v1.6 deployment
# 
class xnat::tomcat::tomcat_xnat16 {

  include xnat
  include xnat::tomcat

  file { "${::xnat::tomcat_catalina_base}/webapps/${::xnat::project_name}":
    ensure => directory,
    owner => $::tomcat::user,
    group => $::tomcat::group,
  }

  # In tomcat6, these steps are done to avoid double-loading the app, noting
  # here for posterity. Doesn't work under tomcat7.
  #
  # 1. mkdir /usr/share/tomcat/webapps2
  # 2. In server.xml: change <Host name=”localhost” appBase=”webapps”  to <Host name=”localhost” appBase=”webapps2”
  # 3. In server.xml: insert the following content into <Host name=”localhost”> element block: <Context path="" docBase=”/usr/share/tomcat6/webapps/xnat”/>

  # Make xnat app the default application via context docBase
  tomcat::config::server::context { "${::xnat::project_name}" :
    catalina_base => $::xnat::tomcat_catalina_base,
    parent_host => 'localhost',
    context_ensure => present,
    doc_base => "${::xnat::project_name}",
    additional_attributes => { "path" => "" },
    require => File["${::xnat::tomcat_catalina_home}/conf"],
  }
}
