# == Class: xnat::tomcat::tomcat_xnat17
#
# Private class to handle Tomcat config for XNAT v1.7 deployment
# 
class xnat::tomcat::tomcat_xnat17 {

  include xnat
  include xnat::tomcat

  # Disable session persistence across Tomcat restarts.  Having that active prevents Tomcat from
  # trying to restore serialized sessions across restarts.
  file_line { 'context_xml_comment':
    path => "${::xnat::tomcat_catalina_base}/conf/context.xml",
    line => '<!-- This file managed by xnat Puppet module -->',
    after => '^<?xml.*',
    ensure => present,
    require => File["${::xnat::tomcat_catalina_home}/conf"],
  }
  file_line { 'disable_session_persistence':
    path => "${::xnat::tomcat_catalina_base}/conf/context.xml",
    line => '<Manager pathname="" />',
    after => '^.*Uncomment this to disable session persistence.*$',
    ensure => present,
    require => File["${::xnat::tomcat_catalina_home}/conf"],
    notify => Exec['remove existing ROOT app'];
  }

  # Add tomcat roles and users
  # TODO: secure this to be appropriate for production use
  tomcat::config::server::tomcat_users {
  'xnat-role-manager-gui':
    ensure        => present,
    catalina_base => $::xnat::tomcat_catalina_base,
    element       => 'role',
    element_name  => 'manager-gui';
  'xnat-role-manager-script':
    ensure        => present,
    catalina_base => $::xnat::tomcat_catalina_base,
    element       => 'role',
    element_name  => 'manager-script';
  'xnat-role-manager-jmx':
    ensure        => present,
    catalina_base => $::xnat::tomcat_catalina_base,
    element       => 'role',
    element_name  => 'manager-jmx';
  'xnat-user-admin':
    ensure        => present,
    catalina_base => $::xnat::tomcat_catalina_base,
    element       => 'user',
    element_name  => 'admin',
    password      => 's3cret',
    roles         => ['manager-gui'];
  'xnat-user-deploy':
    ensure        => present,
    catalina_base => $::xnat::tomcat_catalina_base,
    element       => 'user',
    element_name  => 'deploy',
    password      => 'deploy',
    roles         => ['manager-script', 'manager-jmx'];
  }

  # Move the default ROOT webapps out of the way
  exec { 'remove existing ROOT app':
    command => "rm -rf ${::xnat::tomcat_catalina_base}/webapps/ROOT*",
    path    => ['/usr/bin', '/usr/sbin', '/bin'],
    refreshonly => true,
    before => Class['xnat::install'];
  }
}
