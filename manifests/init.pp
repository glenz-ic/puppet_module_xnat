# == Class: xnat
#
# Base class for XNAT module.  This class presently assumes OpenJDK.  Note this class assumes any
# SSL cert specified has already been deployed by another module/class.
#
# === Parameters
#
# Below are parameters expected by this class. Required vs. optional parameters are noted.
#
# [*db_name*]
#   The name of the PostgreSQL database to use for XNAT.
#   Default: xnat
#
# [*db_user*]
#   The PostgreSQL database user role.
#   Default: xnat
#
# [*db_pass*]
#   Optional, the password to use for PostgreSQL database *db_name*.  It is recommended this
#   parameter be stored in encrypted Hiera for production use, or left undef and database access
#   managed via PostgreSQL HBA.
#   Default: undef
#
# [*db_port*]
#   The PostgreSQL database port.
#   Default: 5432
#
# [*db_host*]
#   Required, the host of the PostgreSQL server.  If this is "localhost" or $::fqdn,
#   PostgreSQL server needs to be installed, and the database/role created per the
#   values given in *db_name*, *db_user*, and *db_pass*.  This class will fail if
#   can't verify DB connectivity.
#
# [*java_opts*]
#   The JAVA_OPTS environment variable for Tomcat.
#   Default: '-Xmx2048m -Xms512m'
#
#   Note this module will insert the following content before whatever is specified above for
#   *java_opts*, depending on XNAT v1.6 or v1.7:
#   v1.6: '-Djavax.sql.DataSource.Factory=org.apache.commons.dbcp.BasicDataSourceFactory'
#   v1.7: '-Djava.awt.headless=true -XX:+UseConcMarkSweepGC -XX:-OmitStackTraceInFastThrow -XX:+CMSIncrementalMode -XX:+CMSClassUnloadingEnabled -Dxnat.home=*xnat_home*'
#
# [*install_groovy*]
#   Whether to include xnat::groovy and install Groovy.  Default: false
#
# [*tomcat_manage_user*]
#   If true, will set the user under which the Tomcat container process runs to *tomcat_user*.
#   Passed to tomcat::install::manage_user.
#   Default: true
#
# [*tomcat_manage_group*]
#   If true, will set the group under which the Tomcat container process runs to *tomcat_group*.
#   Passed to tomcat::install::manage_group.
#   Default: true
#
# [*tomcat_user*]
#   If *tomcat_manage_user* = true, the user under which the Tomcat container process runs. This
#   user will be given ownership of the paths specified in *data_root* and *xnat_home*, along with
#   any paths under that.  Passed to tomcat::user.
#   Default: xnat
#
# [*tomcat_uid*]
#   Optional.  If *tomcat_manage_user* = true, the uid of user under which the Tomcat container process
#   runs. This uid will set in ownership bits of the paths specified in *data_root* and *xnat_home*,
#   along with any paths under that.  Default: whatever the OS selects.
#
# [*tomcat_group*]
#   If *tomcat_manage_group* = true, the group under which the Tomcat container process runs.
#   Passed to tomcat::group.
#   Default: xnat
#
# [*tomcat_gid*]
#   Optional.  If *tomcat_manage_group* = true, the gid of user group under which the Tomcat
#   container process runs. This gid will set in ownership bits of the paths specified in *data_root*
#   and *xnat_home*, along with any paths under that.  Default: whatever the OS selects.
#
# [*tomcat_user_home*]
#   Optional.  If *tomcat_manage_user* = true, the home directory to set for the Tomcat
#   container user.  If left undef, Tomcat user home will be *tomcat_catalina_home*.
#
# [*tomcat_catalina_home*]
#   The CATALINA_HOME path for Tomcat.  When installing Tomcat from package, this parameter must
#   be set to the path created by package install.  Passed to tomcat::catalina_home.
#   Default: /usr/share/tomcat
#
# [*tomcat_catalina_base*]
#   The CATALINA_BASE path for Tomcat.  When installing Tomcat from package, this parameter must
#   be set to the path created by package install.  Passed to tomcat::instance::catalina_base.
#   Default: /usr/share/tomcat
#
# [*tomcat_port*]
#   Not yet implemented.
#
# [*tomcat_web_user*]
#   Not yet implemented.
#
# [*tomcat_web_pass*]
#   Not yet implemented.
#
# [*tomcat_install_from_source*]
#   Whether to install Tomcat from source tarball or package.  False = install from package.
#   Passed to tomcat::install::install_from_source.
#   Default: false
#
# [*tomcat_install_source_url*]
#   If *tomcat_install_from_source* = true, this is the URL for the Tomcat tarball.  Passed to
#   tomcat::install::source_url.
#   Default: http://apache.cs.utah.edu/tomcat/tomcat-7/v7.0.69/bin/apache-tomcat-7.0.69.tar.gz
#
# [*tomcat_package_name*]
#   Optional, if *tomcat_install_from_source* = false, this is the name o the Tomcat package.
#   Passed to tomcat::install::package_name.
#   Default: undef (i.e. use Tomcat module default)
#
# [*apache_frontend*]
#   Whether to deploy an Apache front-end vhost for this XNAT instance.  If set to false, the
#   apache_... params below are ignored.
#   Default: true
#
# [*apache_servername*]
#   The ServerName parameter to use in the Apache front-end vhost.  Unless you have special
#   circumstances (e.g. frontend proxy), this should be the same value as *site_url*.
#   Default: $::fqdn
#
# [*apache_ssl_cert_file*]
#   The SSLCertificateFile parameter to use in the Apache front-end vhost.  Note this
#   assumes cert files are already in place.
#   Default: false (uses puppetlabs/apache module default)
#
# [*apache_ssl_chain_file*]
#   The SSLCertificateChainFile parameter to use in the Apache front-end vhost.  Note
#   this assumes cert files are already in place.
#   Default: false (uses puppetlabs/apache module default)
#
# [*apache_ssl_key_file*]
#   The SSLCertificateKeyFile parameter to use in the Apache front-end vhost.  Note
#   this assumes cert files are already in place.
#   Default: false (uses puppetlabs/apache module default)
#
# [*apache_ssl_ca_file*]
#   The SSLCACertificateFile parameter to use in the Apache front-end vhost.  Note
#   this assumes cert files are already in place.
#   Default: false (uses puppetlabs/apache module default)
# 
# [*apache_ssl_protocol*]
#   The SSLProtocol parameter to use in the Apache SSL config for front-end.
#   Default: [ '-all', '+TLSv1', '+SSLv3']
#
# [*apache_ssl_cipher*]
#   The SSLCipherSuite parameter to use in the Apache SSL config for front-end.
#   Default: 'HIGH:MEDIUM:!aNULL:+SHA1:+MD5:+HIGH:+MEDIUM'
#
#############################
#
# These params apply to XNAT v1.7 and 1.6
#
# [*version*]
#   The version of XNAT to deploy, "1.6" or "1.7".
#   Default: "1.7"
#
# [*install_method*]
#   The installation method to use for deploying XNAT.  Options are:
#   source - only supported for XNAT v1.6, build from source repos
#   WAR - only supported for XNAT v1.7, deploy pre-built WAR file and pipeline zip
#   Default: WAR
#
# [*project_name*]
#   The project name of the XNAT application and the Tomcat webapp name.  On XNAT v1.6 deployments
#   this is also specified as xdat.project.name in the build.properties file.
#   Default: xnat
#
# [*data_root*]
#   The XNAT root data path, the default path under which archive, pipeline, prearchive, cache, ftp,
#   and build directories are created.  This is also where the builder and pipeline source trees
#   retrieved from repos are stored.  Additionally, for XNAT v1.7 this the default path under which
#   *xnat_home* resides.
#   Default: /data/xnat
#
# [*manage_data_root*]
#   Where puppet should try to create the path(s) leading up to *data_root* via 
#   "mkdir -p".  Disable if this conflicts with other modules.  Default: true
#
# [*archive_path*]
#   Optional, a custom archive path.  For XNAT v1.6 this is specified as xdat.archive.location in
#   the build.properties file.  For XNAT v1.7, if this value is defined and 
#   *set_xnat_preferences* = true, this value will be set for archivePath in XNAT preferences upon
#   initial deploy.
#   Default: undef (aka use XNAT default *data_root*/archive)
#
# [*prearchive_path*]
#   Optional, a custom prearchive path.  For XNAT v1.6 this is specified as
#   xdat.prearchive.location in the build.properties file.  For XNAT v1.7, if 
#   *set_xnat_preferences* = true, this value will be set for prearchivePath in XNAT preferences
#   upon initial deploy.
#   Default: undef (aka use XNAT default *data_root*/prearchive)
#
# [*cache_path*]
#   Optional, a custom cache path.  For XNAT v1.6 this is specified as xdat.cache.location in the
#   build.properties file.  For XNAT v1.7, if this value is defined and 
#  *set_xnat_preferences* = true, this value will be set for cachePath in XNAT preferences upon
#   initial deploy.
#   Default: undef (aka use XNAT default *data_root*/cache)
#
# [*ftp_path*]
#   Optional, a custom ftp path.  For XNAT v1.6 this is specified as xdat.ftp.location in the
#   build.properties file.  For XNAT v1.7, if this value is defined and 
#   *set_xnat_preferences* = true, this value will be set for ftpPath in XNAT preferences upon
#   initial deploy.
#   Default: undef (aka use XNAT default *data_root*/ftp)
#
# [*build_path*]
#   Optional, a custom build path.  For XNAT v1.6 this is specified as xdat.build.location in the
#   build.properties file.  For XNAT v1.7, if this value is defined and 
#   *set_xnat_preferences* = true, this value will be set for buildPath in XNAT preferences upon
#   initial deploy.
#   Default: undef (aka use XNAT default *data_root*/build)
#
# [*pipeline_path*]
#   Optional, a custom pipeline path.  For XNAT v1.6, this will be the destination for a checked
#   out copy of the *install_repo_pipeline_source* repo, and it is specified as
#   xdat.pipeline.location in the build.properties file.  For XNAT v1.7, if this value is defined
#   and *set_xnat_preferences* = true, this value will be set for pipelinePath in XNAT preferences
#   upon initial deploy.  Regardless of *set_xnat_preferences*, this is also the destination of the
#   contents built from the *pipeline_zip_url* archive (unless *pipeline_zip_url* is set false).
#   Default: undef (aka use XNAT default *data_root*/pipeline)
#
# [*site_url*]
#   The site URL. For XNAT v1.6 this is the value for xdat.url in build.properties.  For XNAT v1.7,
#   if *set_xnat_preferences* = true, this value will be set for siteURL in XNAT preferences upon
#   initial deploy.  Regardless of *set_xnat_preferences*, it will also be set for xnatUrl in
#   pipeline.gradle.properties.
#   Default: "https://${::fqdn}"
#
# [*mail_admin*]
#   The admin email.  For XNAT v1.6 this is the value for xdat.mail.admin in build.properties. 
#   For XNAT v1.7, if *set_xnat_preferences* = true, this value will be set for adminEmail in XNAT
#   preferences upon initial deploy.  Regardless of *set_xnat_preferences*, it will also be set for
#   adminEmail in pipeline.gradle.properties.
#   Default: root@localhost
#
# [*mail_server*]
#   The email server.  For XNAT v1.6 this is the value for xdat.mail.server in build.properties. 
#   For XNAT v1.7, if *set_xnat_preferences* = true, this value will be set for host in XNAT
#   preferences upon initial deploy.  Regardless of *set_xnat_preferences*, it will also be set for
#   smtpServer in pipeline.gradle.properties.
#   Default: localhost
#
# [*mail_port*]
#   The email server port.  For XNAT v1.6 this is the value for xdat.mail.port in build.properties. 
#   For XNAT v1.7, if *set_xnat_preferences* = true, this value will be set for port in XNAT
#   preferences upon initial deploy.  For pipeline.gradle.properties this is TBD.
#   Default: 25
#
# [*mail_username*]
#   The email server username.  For XNAT v1.6 this is the value for xdat.mail.username in
#   build.properties.  For XNAT v1.7, if *set_xnat_preferences* = true, this value will be set for
#   username in XNAT preferences upon initial deploy.  For pipeline.gradle.properties this is TBD.
#   Default: '' (i.e. blank)
#
# [*mail_password*]
#   The email server password.  For XNAT v1.6 this is the value for xdat.mail.password in
#   build.properties.  For XNAT v1.7, if *set_xnat_preferences* = true, this value will be set for
#   password in XNAT preferences upon initial deploy.  For pipeline.gradle.properties this is TBD.
#   Default: '' (i.e. blank)
#
# [*mail_prefix*]
#   The email subject prefix.  For XNAT v1.6 this is the value for xdat.mail.prefix in
#   build.properties.  For XNAT v1.7, if *set_xnat_preferences* = true, this value will be set for
#   emailPrefix in XNAT preferences upon initial deploy.  For  pipeline.gradle.properties this is
#   TBD.
#   Default: XNAT
#
#############################
#
# These params only relevant to XNAT v1.7, i.e. when $version = '1.7'
#
# [*xnat_war_url*]
#   URL of the XNAT WAR file.  Required when *install_method* = WAR.
#   Default: https://bintray.com/nrgxnat/applications/download_file?file_path=xnat-web-1.7.2.war
#
# [*pipeline_zip_url*]
#   URL of the XNAT pipeline zip file to download and then unpack into *pipeline_path*.  When
#   *install_method* = WAR this can be set to the URL, or to 'false', to not download a pipeline.
#   Setting to false is useful for when a built pipeline already exists.  Note the zip file must
#   include the top-level directory ./xnat-pipeline .
#   Default: https://bintray.com/nrgxnat/applications/download_file?file_path=xnat-pipeline-1.7.2.zip
#
# [*plugins_zip_url*]
#   Optional, URL of a plugins zip file to download and then unpack into *xnat_home*/plugins.
#   Default: undef
#
# [*xnat_home*]
#   Optional, a custom XNAT home path. This is where config, plugins, logs, and work are stored.
#   Default: undef (aka use XNAT default *data_root*/home)
#
# [*site_name*]
#   The site title, aka site ID.  If *set_xnat_preferences* = true, this value will be set for
#   siteID in XNAT preferences upon initial deploy.  Regardless of *set_xnat_preferences*, it will
#   also be set for siteName in pipeline.gradle.properties.
#   Default: XNAT
#
# [*set_xnat_preferences*]
#   Whether to set any values in XNAT preferences database table upon initial deploy, after the
#   webapp has first started up.  Noted above which parameters this affects.  Any XNAT preferences
#   specified are only set once, upon initial deployment.
#   Default: true
#
############################
#
# These params only relevant to XNAT v1.6, i.e. when $version = '1.6'
#
# [*modules_path*]
#   Optional, the path to specify for xdat.modules.location in the build.properties file.  More
#   info TBD.
#   Default: *data_root*/modules
#
# [*install_repo_builder_source*]
#   Required when *install_method* = source.  The mercurial repository URL from which
#   to retrieve the XNAT builder.  Note SSH connections will require root's known_hosts
#   file already be populated with the hostname for the repository server.
#   Examples:
#     https://bitbucket.org/bbuser/xnat_builder
#     https://bbuser:bbpassword@bitbucket.org/bbuser/xnat_builder_private
#     ssh://hg@bitbucket.org/bbuser/xnat_builder
#   Default: https://bitbucket.org/nrg/xnat_builder_1_6dev
#
# [*install_repo_builder_revision*]
#   Optional, the revision to use for *install_repo_builder_source*.
#   Default: undef (aka use latest revision)
#
# [*install_repo_pipeline_source*]
#   Required when *install_method* = source.  The mercurial repository URL from which
#   to retrieve the XNAT pipeline.  Note SSH connections will require root's known_hosts
#   file already be populated with the hostname for the repository server.
#   Examples:
#     https://bitbucket.org/bbuser/xnat_pipeline
#     https://bbuser:bbpassword@bitbucket.org/bbuser/xnat_pipeline_private
#     ssh://hg@bitbucket.org/bbuser/xnat_pipeline
#   Default: https://bitbucket.org/nrg/pipeline_1_6dev
#
# [*install_repo_pipeline_revision*]
#   Optional, the revision to use for *install_repo_pipeline_source*.
#   Default: undef (aka use latest revision)
#
# [*install_repo_project_source*]
#   Optional, the mercurial repository URL from which to retrieve an XNAT project and
#   store in *data_root*/project.  Note SSH connections will require root's known_hosts
#   file already be populated with the hostname for the repository server.
#   Examples:
#     https://bitbucket.org/bbuser/xnat_project
#     https://bbuser:bbpassword@bitbucket.org/bbuser/xnat_project_private
#     ssh://hg@bitbucket.org/bbuser/xnat_project
#
# [*install_repo_project_revision*]
#   Optional, the revision to use for *install_repo_project_source*.
#   Default: undef (aka use latest revision)
#
# Remaining parameter documentation TBD.
#
# === Variables
#
# Custom variables used by this class.
#
# NONE
#
# === Examples
#
#  class { 'xnat':
#    db_name => 'xnat',
#    dn_user => 'xnat',
#    db_pass => hiera("xnat_password"),
#    db_host => 'localhost',
#  }
#
# === Authors
#
# NRG <webmaster@nrg.wustl.edu>
#
# === Copyright
#
# Copyright 2016 NRG.
#
class xnat (
  Variant $version = '1.7',
  String $db_name = 'xnat',
  String $db_user = 'xnat',
  String $db_host,
  Integer $db_port = 5432,
  String $java_opts = '-Xmx2048m -Xms512m',
  Boolean $install_groovy = false,
  Boolean $tomcat_manage_user = true,
  Boolean $tomcat_manage_group = true,
  String $tomcat_user = 'xnat',
  String $tomcat_group = 'xnat',
  String $tomcat_catalina_base = '/usr/share/tomcat',
  String $tomcat_catalina_home = '/usr/share/tomcat',
  Boolean $tomcat_install_from_source = false,
  String $tomcat_install_source_url = 'http://apache.cs.utah.edu/tomcat/tomcat-7/v7.0.72/bin/apache-tomcat-7.0.72.tar.gz',
  Boolean $apache_frontend = true,
  String $apache_servername = "${::fqdn}",
  Array $apache_ssl_protocol = [ '-all', '+TLSv1', '+SSLv3'],
  String $apache_ssl_cipher = 'HIGH:MEDIUM:!aNULL:+SHA1:+MD5:+HIGH:+MEDIUM',
  Variant $apache_ssl_cert_file = false,
  Variant $apache_ssl_key_file = false,
  Variant $apache_ssl_chain_file = false,
  Variant $apache_ssl_ca_file = false,
  String $data_root = '/data/xnat',
  Boolean $manage_data_root = true,
  String $project_name = 'xnat',
  Integer $mail_port = 25,
  String $mail_server ='localhost',
  String $mail_username = '',
  Variant $mail_password = '',
  String $mail_admin = 'root@localhost',
  String $mail_prefix = 'XNAT',
  String $install_method = 'WAR',
  String $install_repo_builder_source = 'https://bitbucket.org/nrg/xnat_builder_1_6dev',
  String $install_repo_pipeline_source = 'https://bitbucket.org/nrg/pipeline_1_6dev',
  String $xnat_war_url = 'https://bintray.com/nrgxnat/applications/download_file?file_path=xnat-web-1.7.2.war',
  Variant $pipeline_zip_url = 'https://bintray.com/nrgxnat/applications/download_file?file_path=xnat-pipeline-1.7.2.zip',
  String $site_name = 'XNAT',
  String $site_url = "https://${::fqdn}",
  Optional[Variant] $db_pass = undef,
  Optional[String] $xnat_home = undef,
  Optional[String] $archive_path = undef,
  Optional[String] $prearchive_path = undef,
  Optional[String] $cache_path = undef,
  Optional[String] $ftp_path = undef,
  Optional[String] $build_path = undef,
  Optional[String] $pipeline_path = undef,
  Optional[String] $modules_path = undef,
  Optional[String] $install_repo_builder_revision = undef,
  Optional[String] $install_repo_pipeline_revision = undef,
  Optional[String] $install_repo_project_source = undef,
  Optional[String] $install_repo_project_revision = undef,
  Optional[Variant] $plugins_zip_url = undef,
  Optional[Variant] $tomcat_uid = undef,
  Optional[Variant] $tomcat_gid = undef,
  Optional[String] $tomcat_web_user = undef,
  Optional[String] $tomcat_web_pass = undef,
  Optional[String] $tomcat_user_home = undef,
  Optional[Integer] $tomcat_port = undef,
  Optional[String] $tomcat_package_name = undef,
  Optional[String] $catalina_tmp_dir = undef,
  ){

  # Additional java module params can be passed via hiera
  include java

  # Populate some default params if needed
  $xnat_home_real = $xnat_home ? {
    undef => "${data_root}/home",
    default => $xnat_home,
  }
  $archive_path_real = $archive_path ? {
    undef => "${data_root}/archive",
    default => $archive_path,
  }
  $prearchive_path_real = $prearchive_path ? {
    undef => "${data_root}/prearchive",
    default => $prearchive_path,
  }
  $cache_path_real = $cache_path ? {
    undef => "${data_root}/cache",
    default => $cache_path,
  }
  $ftp_path_real = $ftp_path ? {
    undef => "${data_root}/ftp",
    default => $ftp_path,
  }
  $build_path_real = $build_path ? {
    undef => "${data_root}/build",
    default => $build_path,
  }
  $pipeline_path_real = $pipeline_path ? {
    undef => "${data_root}/pipeline",
    default => $pipeline_path,
  }
  $modules_path_real = $modules_path ? {
    undef => "${data_root}/modules",
    default => $modules_path,
  }

  # Misc package dependencies
  $xnat_pkglist = $::osfamily ? {
    'RedHat' => [ "curl", "unzip", "zip", "mercurial" ],
    'Debian' => [ "curl", "unzip", "zip", "mercurial" ],
    default => [ ],
  }
  ensure_packages($xnat_pkglist, {'ensure' => 'latest'})

  # Create necesssary directories
  $xnat_dirs = $version ? {
    '1.6' => [ $data_root, $archive_path_real, $prearchive_path_real,
               $cache_path_real, $ftp_path_real, $build_path_real,
               $pipeline_path_real, $modules_path_real ],
    '1.7' => [ $data_root, $xnat_home_real, $archive_path_real, $prearchive_path_real,
               $cache_path_real, $ftp_path_real, $build_path_real,
               $pipeline_path_real, "${data_root}/src" ],
  }
  
  $xnat_dirs.each | String $xnat_dir | {
    if ($xnat_dir == $data_root) {
      if $manage_data_root {
        ensure_resource('common::mkdir_p', $xnat_dir)
        $dir_require = Common::Mkdir_p[$xnat_dir]
      }
      else {
        $dir_require = undef
      }
    }
    else {
      ensure_resource('common::mkdir_p', $xnat_dir)
      $dir_require = Common::Mkdir_p[$xnat_dir]
    }
    $dir_resources = {
      'ensure' => 'directory',
      'owner' => $tomcat_user,
      'group' => $tomcat_group,
      'require' => $dir_require,
    }
    ensure_resource('file', $xnat_dir, $dir_resources)
  }

  # Ensure JAVA_HOME environment var is set (since puppetlabs-java doesn't do this).
  # TODO: Generalize this so it works with Oracle JDK and other OSes
    
  $java_home = $::osfamily ? {
    'RedHat' => '/usr/lib/jvm/java',
    'Debian' => '/usr/bin/java',
    default => '/usr/lib/jvm/java',
  }

  file {
    "/etc/profile.d/java_home.sh":
      owner		=> root,
      group		=> root,
      mode		=> "0755",
      replace		=> true,
      content		=> template("xnat/java_home.sh.erb"),
      require           => Class['java'];
    "/etc/profile.d/java_home.csh":
      owner		=> root,
      group		=> root,
      mode		=> "0755",
      replace		=> true,
      content		=> template("xnat/java_home.csh.erb"),
      require           => Class['java'];
  }

  # Install Groovy if enabled. Additional params can be passed via hiera.
  if $install_groovy {
    include xnat::groovy
  }

  # Verify Postgresql DB connectivity
  postgresql::validate_db_connection { 'validate XNAT pgsql connection':
    database_host     => $db_host,
    database_port     => $db_port,
    database_username => $db_user,
    database_password => $db_pass,
    database_name     => $db_name,
  } 
  
  # Set up Apache frontend if enabled
  if $apache_frontend {
    class { 'xnat::apache_frontend': }
  }

  # Set up Tomcat container
  class { 'xnat::tomcat': }

  # Now install XNAT based on method specified
  class { 'xnat::install': }

  # Build/deploy XNAT once installed
  class { 'xnat::build': }

  # Dependencies
  Postgresql::Validate_db_connection['validate XNAT pgsql connection'] -> Class['xnat::build']
  Class['xnat::install'] -> Class['xnat::build']
  Tomcat::Install["$tomcat_catalina_home"] -> Class['xnat::build']
  Class['xnat::build'] ~> Tomcat::Service["${project_name}"]

}
