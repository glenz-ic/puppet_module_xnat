# == Class: xnat::install::war_xnat17
#
# Private class for retrieving XNAT v1.7 WAR and other artifacts
#
class xnat::install::war_xnat17 {
 
  include xnat
  include wget
  include tomcat

  # Retrieve XNAT WAR from URL specified, unless already downloaded
  $fetch_dest = "${::xnat::data_root}/src/"
  $war_filename = basename($::xnat::xnat_war_url)
  wget::fetch { 'xnat_war':
    source => $::xnat::xnat_war_url,
    destination => $fetch_dest,
    execuser => $::xnat::tomcat_user,
    unless => "test -f ${fetch_dest}${war_filename}",
    notify => Exec['xnat_deploy_war'];
  }

  # Retrieve pipeline zip, if specified
  if $::xnat::pipeline_zip_url {
    $pipeline_filename = join(['pipeline_', basename($::xnat::pipeline_zip_url)])
    wget::fetch { 'pipeline_file':
      source => $::xnat::pipeline_zip_url,
      destination => "${fetch_dest}${pipeline_filename}",
      execuser => $::xnat::tomcat_user,
      unless => "test -f ${fetch_dest}${pipeline_filename}",
      notify => Exec['unzip_pipeline'];
    }
  }

  # Retrieve plugins zip, if specified
  if $::xnat::plugins_zip_url {
    $plugins_filename = join(['plugins_', basename($::xnat::plugins_zip_url)])
    wget::fetch { 'plugins_file':
      source => $::xnat::plugins_zip_url,
      destination => "${fetch_dest}${plugins_filename}",
      execuser => $::xnat::tomcat_user,
      unless => "test -f ${fetch_dest}${plugins_filename}",
      notify => Exec['unzip_plugins'];
    }
  }

}

