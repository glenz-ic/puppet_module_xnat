# == Class: xnat::install::source_xnat16
#
# Private class for retrieving XNAT v1.6 from source repos
#
class xnat::install::source_xnat16 {
 
  include xnat

  # Retrieve XNAT artifacts from repos
  file {
    "${::xnat::data_root}/builder":
      ensure => directory,
      owner => $::xnat::tomcat_user,
      group => $::xnat::tomcat_group,
  }
  vcsrepo {
    "${::xnat::data_root}/builder":
      ensure => present,
      provider => hg,
      user => $::xnat::tomcat_user,
      revision => $::xnat::install_repo_builder_revision,
      source => $::xnat::install_repo_builder_source;
    $::xnat::pipeline_path_real:
      ensure => present,
      provider => hg,
      user => $::xnat::tomcat_user,
      revision => $::xnat::install_repo_pipeline_revision,
      source => $::xnat::install_repo_pipeline_source;
  }
  ->
  # Currently must patch project.properties to include direct reference to artifactoryonline.com;
  # Maven v1.0.2 is too stupid to understand 302 redirects.
  file_line {
    'project_properties_artifactory':
      path => "${::xnat::data_root}/builder/project.properties",
      match => '^maven\.repo\.remote(\s?)\=',
      line => 'maven.repo.remote=http://maven.xnat.org/xnat-maven1,http://repo1.maven.org/maven,http://mirrors.ibiblio.org/pub/mirrors/maven,http://mirrors.ibiblio.org/maven,https://nrgxnat.jfrog.io/nrgxnat/xnat-maven1';
  }
  if $::xnat::install_repo_project_source {
    file { "${::xnat::data_root}/project":
      ensure => directory,
      owner => $::xnat::tomcat_user,
      group => $::xnat::tomcat_group,    
    }
    vcsrepo { 
    "${::xnat::data_root}/project":
      ensure => present,
      provider => hg,
      user => $::xnat::tomcat_user,
      revision => $::xnat::install_repo_project_revision,
      source => $::xnat::install_repo_project_source;
    }  
  }
}
