# == Class: xnat::build
#
# Private class for XNAT build/deployment
#
class xnat::build {

  include xnat

  # Chose the build class based on method and version specified
  case $::xnat::version {
    '1.6': {
      case $::xnat::install_method {
        "source": {
          class{ 'xnat::build::build_xnat16': }
          Class['xnat::build::build_xnat16'] ~> Class['xnat::build']
          Postgresql::Validate_db_connection['validate XNAT pgsql connection'] -> Class['xnat::build::build_xnat16']
        }
        default: {
          fail("XNAT puppet module support for installation method ${::xnat::install_method} and version ${::xnat::version} is unsupported.")
        }
      }
    }
    '1.7': {
       case $::xnat::install_method {
        "WAR": {
          class{ 'xnat::build::build_xnat17': }
          Class['xnat::build::build_xnat17'] ~> Class['xnat::build']
          Postgresql::Validate_db_connection['validate XNAT pgsql connection'] -> Class['xnat::build::build_xnat17']
        }
        default: {
          fail("XNAT puppet module support for installation method ${::xnat::install_method} and version ${::xnat::version} is unsupported.")
        }
      }     
    }
  }

}
