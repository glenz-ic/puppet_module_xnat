# == Class: xnat::build::build_xnat16
#
# Private class for XNAT v.16 build/deployment from source
#
class xnat::build::build_xnat16 {

  include xnat
  include xnat::build
  include tomcat
  include postgresql::client
  include java
  
  # Create XNAT build.properties from sample and populate appropriately
  $build_properties_path = $::xnat::install_method ? {
    "source" => "${::xnat::data_root}/builder",
    default => $::xnat::data_root,
  }
  exec { 'xnat_copy_build_properties':
    command => 'cp build.properties.sample build.properties',
    path    => ['/usr/bin', '/usr/sbin', '/bin'],
    cwd => $build_properties_path,
    user => $::xnat::tomcat_user,
    creates => "${build_properties_path}/build.properties";
  }
  $file_line_defaults = {
    path => "${build_properties_path}/build.properties",
    require => Exec['xnat_copy_build_properties'],
    before => Exec['xnat_setup'],
    replace => true,
  }
  $file_line_data = {
    'xnat_maven_appserver_home' => {
      line => "maven.appserver.home=${::xnat::tomcat_catalina_base}",
      match => '^maven\.appserver\.home(\s?)\=',
    },
    'xnat_xdat_project_db_driver' => {
      line => 'xdat.project.db.driver=org.postgresql.Driver',
      match => '^xdat\.project\.db\.driver(\s?)\=',
    },
    'xnat_xdat_project_db_connection_string' => {
      line => "xdat.project.db.connection.string=jdbc:postgresql://${::xnat::db_host}/${::xnat::db_name}",
      match => '^xdat\.project\.db\.connection\.string(\s?)\=',
    },
    'xnat_xdat_project_db_user' => {
      line => "xdat.project.db.user=${::xnat::db_user}",
      match => '^xdat\.project\.db\.user(\s?)\=',
    },
    'xnat_xdat_project_db_password' => {
      line => "xdat.project.db.password=${::xnat::db_pass}",
      match => '^xdat\.project\.db\.password(\s?)\=',
    },
    'xnat_project_name' => {
      line => "xdat.project.name=${::xnat::project_name}",
      match => '^xdat\.project\.name(\s?)\=',
    },
    'xnat_xdat_archive_location' => {
      line => "xdat.archive.location=${::xnat::archive_path_real}",
      match => '^xdat\.archive\.location(\s?)\=',
    },
    'xnat_xdat_prearchive_location' => {
      line => "xdat.prearchive.location=${::xnat::prearchive_path_real}",
      match => '^xdat\.prearchive\.location(\s?)\=',
    },
    'xnat_xdat_cache_location' => {
      line => "xdat.cache.location=${::xnat::cache_path_real}",
      match => '^xdat\.cache\.location(\s?)\=',
    },
    'xnat_xdat_ftp_location' => {
      line => "xdat.ftp.location=${::xnat::ftp_path_real}",
      match => '^xdat\.ftp\.location(\s?)\=',
    },
    'xnat_xdat_build_location' => {
      line => "xdat.build.location=${::xnat::build_path_real}",
      match => '^xdat\.build\.location(\s?)\=',
    },
    'xnat_xdat_pipeline_location' => {
      line => "xdat.pipeline.location=${::xnat::pipeline_path_real}",
      match => '^xdat\.pipeline\.location(\s?)\=',
    },
    'xnat_xdat_modules_location' => {
      line => "xdat.modules.location=${::xnat::modules_path_real}",
      match => '^xdat\.modules\.location(\s?)\=',
    },
    'xnat_xdat_mail_port' => {
      line => "xdat.mail.port=${::xnat::mail_port}",
      match => '^xdat\.mail\.port(\s?)\=',
    },
    'xnat_xdat_mail_server' => {
      line => "xdat.mail.server=${::xnat::mail_server}",
      match => '^xdat\.mail\.server(\s?)\=',
    },
    'xnat_xdat_mail_admin' => {
      line => "xdat.mail.admin=${::xnat::mail_admin}",
      match => '^xdat\.mail\.admin(\s?)\=',
    },
    'xnat_xdat_mail_username' => {
      line => "xdat.mail.username=${::xnat::mail_username}",
      match => '^xdat\.mail\.username(\s?)\=',
    },
    'xnat_xdat_mail_password' => {
      line => "xdat.mail.password=${::xnat::mail_password}",
      match => '^xdat\.mail\.password(\s?)\=',
    },
    'xnat_xdat_mail_prefix' => {
      line => "xdat.mail.prefix=${::xnat::mail_prefix}",
      match => '^xdat\.mail\.prefix(\s?)\=',
    },
    'xnat_xdat_url' => {
      line => "xdat.url=${::xnat::site_url}",
      match => '^xdat\.url(\s?)\=',
    }
  }
  create_resources('file_line', $file_line_data, $file_line_defaults)

  # Now ready to run setup script
  exec { 'xnat_setup':
    creates => "${build_properties_path}/deployments/${::xnat::project_name}",
    command => "${build_properties_path}/bin/setup.sh",
    environment => [ "JAVA_HOME=${::xnat::java_home}" ],
    user => $::xnat::tomcat_user,
    cwd => $build_properties_path,
    require => Class['java'],
    notify => Exec['xnat_import_schema'],
  }

  # Import DB schema
  $xnat_schema_file = "${build_properties_path}/deployments/${::xnat::project_name}/sql/${::xnat::project_name}.sql"
  exec { 'xnat_import_schema':
    require => [ Exec['xnat_setup'], Class['postgresql::client'] ],
    environment => [ "PGPASSWORD=${::xnat::db_pass}" ],
    command => "psql -U ${::xnat::db_user} -h ${::xnat::db_host} -f ${xnat_schema_file} ${::xnat::db_name}",
    path    => ['/usr/bin', '/usr/sbin', '/bin'],
    unless => "psql -U ${::xnat::db_user} -h ${::xnat::db_host} -t -c '\dt' ${::xnat::db_name} | grep -v 'No relations found' -q",
    refreshonly => true,
    notify => Exec['xnat_create_users'],
  }

  # Create initial XNAT users, incl. admin/admin
  exec { 'xnat_create_users':
    command => "${build_properties_path}/bin/StoreXML -l security/security.xml -allowDataDeletion true",
    cwd => "${build_properties_path}/deployments/${::xnat::project_name}",
    user => $::xnat::tomcat_user,
    refreshonly => true,
    notify => Exec['xnat_create_variable_sets'],
  }

  # Create variable sets
  exec { 'xnat_create_variable_sets':
    command => "${build_properties_path}/bin/StoreXML -dir ./work/field_groups -u admin -p admin -allowDataDeletion true",
    cwd => "${build_properties_path}/deployments/${::xnat::project_name}",
    user => $::xnat::tomcat_user,
    refreshonly => true,
    notify => Exec['xnat_deploy_webapp'],
  }
  
  # If project repo specified, copy in modules to builder directory
  if $::xnat::install_repo_project_source {
    exec { 'copy_from_xnat_project_repo' :
      command => "cp -ra ${::xnat::data_root}/project/* ${build_properties_path}/projects/${::xnat::project_name}/",
      path    => ['/usr/bin', '/usr/sbin', '/bin'],
      user => $::xnat::tomcat_user,
      require => Exec['xnat_setup'],
      before => Exec['xnat_deploy_webapp'],
    }
  }
  
  # Deploy webapp
  exec { 'xnat_deploy_webapp' :
    command => "${build_properties_path}/bin/update.sh -Ddeploy=true",
    cwd => $build_properties_path,
    user => $::xnat::tomcat_user,
    environment => [ "JAVA_HOME=${::xnat::java_home}" ],
    refreshonly => true,
    notify => Exec['xnat_chown_webapp'],
  }

  # Fix ownership of deployed webapp
  exec { 'xnat_chown_webapp' :
    command => "chown -R ${::tomcat::user}.${::tomcat::group} ${::xnat::tomcat_catalina_base}/webapps/${::xnat::project_name}",
    path    => ['/usr/bin', '/usr/sbin', '/bin'],
    refreshonly => true,
    notify => Exec['xnat_update_schema'],
  }

  # Run updated SQL
  $xnat_schema_update = "${build_properties_path}/deployments/${::xnat::project_name}/sql/${::xnat::project_name}-update.sql"
  exec { 'xnat_update_schema' :
    require => Class['postgresql::client'],
    environment => [ "PGPASSWORD=${::xnat::db_pass}" ],
    command => "psql -U ${::xnat::db_user} -h ${::xnat::db_host} -f ${xnat_schema_update} ${::xnat::db_name}",
    path    => ['/usr/bin', '/usr/sbin', '/bin'],
    refreshonly => true,
  }
  
}
