# == Class: xnat::build::build_xnat17
#
# Private class for XNAT v.17 deployment from WAR
#
class xnat::build::build_xnat17 {

  include xnat
  include xnat::build
  include tomcat
  include postgresql::client
  include java

  # Create directories in XNAT home, etc
  $home_dirs = [ "${::xnat::xnat_home_real}/config", "${::xnat::xnat_home_real}/logs",
                 "${::xnat::xnat_home_real}/plugins", "${::xnat::xnat_home_real}/work",
                 "${::xnat::data_root}/src/pipeline" ]
  file { $home_dirs:
    ensure => directory,
    owner => $::xnat::tomcat_user,
    group => $::xnat::tomcat_group,
    require => File[$::xnat::xnat_home_real],
  }

  # XNAT config
  file { 
    "${::xnat::xnat_home_real}/config/xnat-conf.properties" :
      ensure => file,
      owner => $::xnat::tomcat_user,
      group => $::xnat::tomcat_group,
      content => template('xnat/xnat-conf.properties.erb'),
      require => File["${::xnat::xnat_home_real}/config"];
  }

  # Pipeline config and build, if pipeline zip provided
  if $::xnat::pipeline_zip_url {
    file { 
      "${::xnat::data_root}/src/pipeline/xnat-pipeline/gradle.properties" :
        ensure => file,
        owner => $::xnat::tomcat_user,
        group => $::xnat::tomcat_group,
        content => template('xnat/pipeline.gradle.properties.erb'),
        require => File["${::xnat::data_root}/src/pipeline"],
        notify => Exec['build_pipeline'];
    }
    $pipeline_filename = join(['pipeline_', basename($::xnat::pipeline_zip_url)])
    exec {
      'unzip_pipeline':
        # This actually just removes existing pipeline source
        command => "/bin/rm -rf ${::xnat::data_root}/src/xnat-pipeline",
        refreshonly => true,
        notify => Exec['really_unzip_pipeline'];
      'really_unzip_pipeline':
        # Assumed pipeline zip contains top-level directory ./xnat-pipeline
        command => "unzip -qo ${::xnat::data_root}/src/${pipeline_filename}",
        path    => ['/usr/bin', '/usr/sbin', '/bin'],
        user => $::xnat::tomcat_user,
        cwd => "${::xnat::data_root}/src/pipeline",
        refreshonly => true,
        notify => [ File["${::xnat::data_root}/src/pipeline/xnat-pipeline/gradle.properties"],
                    Exec['build_pipeline'] ];
      'build_pipeline':
        command => "${::xnat::data_root}/src/pipeline/xnat-pipeline/gradlew -q",
        user => $::xnat::tomcat_user,
        environment => [ "JAVA_HOME=${::xnat::java_home}" ],
        cwd => "${::xnat::data_root}/src/pipeline/xnat-pipeline",
        refreshonly => true,
        require => File["${::xnat::data_root}/src/pipeline/xnat-pipeline/gradle.properties"],
        notify => Exec['xnat_deploy_war'];
    }
  }

  # Unpack plugins, if plugins zip provided
  if $::xnat::plugins_zip_url {
    $plugins_filename = join(['plugins_', basename($::xnat::plugins_zip_url)])
    exec {
      'unzip_plugins':
        command => "unzip -qo ${::xnat::data_root}/src/${plugins_filename}",
        path    => ['/usr/bin', '/usr/sbin', '/bin'],
        user => $::xnat::tomcat_user,
        cwd => "${::xnat::xnat_home_real}/plugins",
        refreshonly => true,
        notify => Exec['xnat_deploy_war'];
    }
  }

  # TODO: Deploy a custom values for archive_path, cache_path, etc to site-config.properties
  
  # Copy WAR to tomcat webapps
  $war_filename = basename($::xnat::xnat_war_url)
  exec {
    'xnat_deploy_war' :
      command => "cp ${::xnat::data_root}/src/${war_filename} ${::xnat::tomcat_catalina_base}/webapps/ROOT.war",
      path    => ['/usr/bin', '/usr/sbin', '/bin'],
      user => $::xnat::tomcat_user,
      refreshonly => true,
      notify => Tomcat::Service["${::xnat::project_name}"],
      require => File["${::xnat::xnat_home_real}/config/xnat-conf.properties"];
  }
  
}
