# == Class: xnat::tomcat
#
# Private class for XNAT tomcat container
# 
# TODO: Verify for cases beyond installing Tomcat7 via package on CentOS6 and CentOS7.
# TODO: Extend to support Tomcat8 (and Tomcat6?)
#
class xnat::tomcat {

  include xnat

  $tomcat_user_real = $::xnat::tomcat_manage_user ? {
    false => undef,
    default => $::xnat::tomcat_user,
  }
  $tomcat_group_real = $::xnat::tomcat_manage_group ? {
    false => undef,
    default => $::xnat::tomcat_group,
  }

  # Set tomcat user home directory per XNAT version and/or value of $tomcat_user_home
  if $::xnat::tomcat_user_home {
    $tomcat_user_home_real = $::xnat::tomcat_user_home
  }
  else {
    $tomcat_user_home_real = $::xnat::version ? {
      '1.6' => $::xnat::tomcat_catalina_home,
      '1.7' => $::xnat::xnat_home_real, 
    }
  }

  # Set JAVA_OPTS per XNAT version
  $java_opts_real =  $::xnat::version ? {
    '1.6' => "-Djavax.sql.DataSource.Factory=org.apache.commons.dbcp.BasicDataSourceFactory ${::xnat::java_opts}",
    '1.7' => "-Djava.awt.headless=true -XX:+UseConcMarkSweepGC -XX:-OmitStackTraceInFastThrow -XX:+CMSIncrementalMode -XX:+CMSClassUnloadingEnabled -Dxnat.home=${::xnat::xnat_home_real} ${::xnat::java_opts}",
  }
  
  # TODO for XNAT v1.7, parameter to enable appending this to JAVA_OPTS for debugging
  # "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000"

  # Additional java and tomcat module params can be passed via hiera
  include java
  class { 'tomcat':
    user => $tomcat_user_real,
    group => $tomcat_group_real,
    catalina_home => $::xnat::tomcat_catalina_home,
  }

  # TODO: Ensure this works on all relevant CentOS versions, Ubuntu
  $tomcat_sysconfig = $::osfamily ? {
    'RedHat' => '/etc/sysconfig/tomcat',
    'Debian' => '/etc/default/tomcat7',
    default => '/etc/default/tomcat7',
  }
  $tomcat_etc_config = $::osfamily ? {
    'RedHat' => '/etc/tomcat',
    'Debian' => '/etc/tomcat7',
    default => '/etc/tomcat7',
  }
  
  $tomcat_package_name = $::osfamily ? {
    'RedHat' => 'tomcat',
    'Debian' => 'tomcat7',
    default => 'tomcat',
  }

  # Note this resource may not notify if tomcat package already installed
  tomcat::install { $::xnat::tomcat_catalina_home:
    install_from_source => $::xnat::tomcat_install_from_source,
    source_url => $::xnat::tomcat_source_url,
    package_name => $tomcat_package_name,
    manage_user => $::xnat::tomcat_manage_user,
    manage_group => $::xnat::tomcat_manage_group,
    user => $tomcat_user_real,
    group => $tomcat_group_real,
    notify => Exec['chown catalina_base'],
  }

  # Modify Tomcat startup scripts as needed
  if (! $::xnat::tomcat_install_from_source) {
    # Tomcat7 installed from EPEL repo for RHEL/CentOS 6 requires patched init.d script.
    if ($::operatingsystemmajrelease == '6' and $::osfamily == 'RedHat' ) {
      file { '/etc/init.d/tomcat' :
        ensure => file,
        mode => '755',
        source => 'puppet:///modules/xnat/tomcat7.initd.centos6',
        require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
        before => Tomcat::Service["${::xnat::project_name}"];
      }
    }
    # Tomcat7 installed from RHEL/CentOS rpm requires patched /usr/lib/systemd/system/tomcat.service
    elsif ($::operatingsystemmajrelease == '7' and $::osfamily == 'RedHat' ) {
      $tomcat_user_template = $::tomcat::user
      $tomcat_group_template = $::tomcat::group
      file { '/usr/lib/systemd/system/tomcat.service' :
        ensure => file,
        content  => template('xnat/tomcat7.service.centos7.erb'),
        require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
        before => Tomcat::Service["${::xnat::project_name}"],
        notify => Exec['reload tomcat.service'];
      }
      exec { 'reload tomcat.service' :
        command => '/bin/systemctl daemon-reload',
        refreshonly => true,
        before => Tomcat::Service["${::xnat::project_name}"];
      }
    }

  }

  # Tomcat module doesn't create some resources when installed from package
  if $::xnat::tomcat_manage_user {
    ensure_resource('user', $tomcat_user_real, {
      ensure => present,
      gid    => $tomcat_group_real,
      home   => $tomcat_user_home_real,
      before => Exec['chown catalina_base'],
      })
      file_line { "tomcat_sysconfig_user":
        line => "TOMCAT_USER=\"${tomcat_user_real}\"",
        path => $tomcat_sysconfig,
        match => "^TOMCAT_USER=.*",
        require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
        before => Tomcat::Service["${::xnat::project_name}"],
      }
    if $::xnat::tomcat_uid {
      User <| title == $tomcat_user_real |> { uid => $::xnat::tomcat_uid }
    }
  }
  if $::xnat::tomcat_manage_group {
    ensure_resource('group', $tomcat_group_real, {
      ensure => present,
      before => Exec['chown catalina_base'],
      })
      file_line { "tomcat_sysconfig_group":
        line => "TOMCAT_GROUP=\"${tomcat_group_real}\"",
        path => $tomcat_sysconfig,
        match => "^TOMCAT_GROUP=.*",
        require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
        before => Tomcat::Service["${::xnat::project_name}"],
      }
    if $::xnat::tomcat_gid {
      Group <| title == $tomcat_group_real |> { gid => $::xnat::tomcat_gid }
    }
  }
  ensure_resource('file', $::xnat::tomcat_catalina_home, {
    ensure => directory,
    owner => $tomcat_user_real,
    group => $tomcat_group_real
    })
  ensure_resource('file', $::xnat::tomcat_catalina_base, {
    ensure => directory,
    owner => $tomcat_user_real,
    group => $tomcat_group_real
    })

  # Fix/verify link to /etc/tomcat
  exec { "move old tomcat conf":
    command => "mv ${::xnat::tomcat_catalina_home}/conf ${::xnat::tomcat_catalina_home}/conf.orig",
    creates => "${::xnat::tomcat_catalina_home}/conf.orig",
    path    => ['/usr/bin', '/usr/sbin', '/bin'],
    unless => "test -L ${::xnat::tomcat_catalina_home}/conf",
    require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
    before => Tomcat::Service["${::xnat::project_name}"];
  }
  ->
  file { "${::tomcat::catalina_home}/conf":
    ensure => link,
    target => $tomcat_etc_config,
  }

  # Deploy tomcat service
  file_line { "tomcat_sysconfig_comment":
    line => "# This file managed by xnat Puppet module",
    path => $tomcat_sysconfig,
    after => "^# a per-service manner.*",
    require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
  }
  file_line { "tomcat_sysconfig_java_opts":
    line => "JAVA_OPTS=\"$java_opts_real\"",
    path => $tomcat_sysconfig,
    match => "^JAVA_OPTS=.*",
    notify => Exec['chown catalina_base'],
    require => Tomcat::Install["${::xnat::tomcat_catalina_home}"],
  }
  ->
  tomcat::instance { "${::xnat::project_name}":
    catalina_base => $::xnat::tomcat_catalina_base,
    manage_service => false,
  }
  ->
  tomcat::service { "${::xnat::project_name}":
    use_jsvc     => false,
    use_init     => true,
    service_name => 'tomcat';
  }
 
  # Ensure correct ownership of CATALINA_BASE
  # Ignore error codes; often problems with missing log4j.jar symlinks
  exec { 'chown catalina_base':
    command => "chown -RLf ${::tomcat::user}.${::tomcat::group} $::xnat::tomcat_catalina_base/{.,lib,logs,logs/.,temp,work,temp/..,work/..,webapps/.}",
    path    => ['/usr/bin', '/usr/sbin', '/bin'],
    returns => [0, 1],
    timeout => '18000',
    refreshonly => true,
    before => Tomcat::Service["${::xnat::project_name}"],
  }
  
  # Further Tomcat config depends on XNAT version
  case $::xnat::version {
    '1.6': {
      class { 'xnat::tomcat::tomcat_xnat16': }
      Tomcat::Install["${::xnat::tomcat_catalina_home}"] -> Class['xnat::tomcat::tomcat_xnat16']
    }
    
    '1.7': {
      class { 'xnat::tomcat::tomcat_xnat17': }
      Tomcat::Install["${::xnat::tomcat_catalina_home}"] -> Class['xnat::tomcat::tomcat_xnat17']
    }   
  }
}
