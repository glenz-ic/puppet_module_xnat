# == Class: xnat::apache_frontend
#
# Private class for XNAT Apache frontend
#
class xnat::apache_frontend {

  include xnat
  include apache
  include firewall
  
  $ssl_cert_real = $::xnat::apache_ssl_cert_file ? {
    false => undef,
    default => $::xnat::apache_ssl_cert_file,
  }
  $ssl_key_real = $::xnat::apache_ssl_key_file ? {
    false => undef,
    default => $::xnat::apache_ssl_key_file,
  }
  $ssl_chain_real = $::xnat::apache_ssl_chain_file ? {
    false => undef,
    default => $::xnat::apache_ssl_chain_file,
  }
  $ssl_ca_real = $::xnat::apache_ssl_ca_file ? {
    false => undef,
    default => $::xnat::apache_ssl_ca_file,
  }

  # Open port 443 HTTPS and port 80 HTTP
  firewall {
    '100 allow HTTPS access':
      dport    => 443,
      ctstate  => 'NEW',
      proto    => tcp,
      action   => accept,
      chain    => 'INPUT';
    '100 allow HTTP access':
      dport    => 80,
      ctstate  => 'NEW',
      proto    => tcp,
      action   => accept,
      chain    => 'INPUT';
  }

  # Install mod_proxy_ajp
  class { 'apache::mod::proxy_ajp': }
  
  # Set up the Apache SSL vhost and redirect from HTTP
  class { 'apache::mod::ssl':
    ssl_protocol => $::xnat::apache_ssl_protocol,
    ssl_cipher => $::xnat::apache_ssl_cipher,
  }
  apache::vhost { "${::xnat::apache_servername}_non-ssl":
    servername      => $::xnat::apache_servername,
    port            => '80',
    redirect_status => 'permanent',
    redirect_dest   => "https://${::xnat::apache_servername}/",
    manage_docroot => false,
    docroot => false,
  }
  apache::vhost { "${::xnat::apache_servername}_ssl":
    servername => $::xnat::apache_servername,
    port       => '443',
    ssl        => true,
    ssl_cert => $ssl_cert_real,
    ssl_key  => $ssl_key_real,
    ssl_chain => $ssl_chain_real,
    ssl_ca => $ssl_ca_real,
    manage_docroot => false,
    docroot => false,
    proxy_pass => [ { 'path' => '/', 'url' => 'ajp://localhost:8009/',
                    'reverse_urls' => [ 'http://localhost:8080/' ] } ],
  }
  Class['apache::mod::proxy_ajp'] -> Apache::Vhost["${::xnat::apache_servername}_ssl"]
  Class['apache::mod::ssl'] -> Apache::Vhost["${::xnat::apache_servername}_ssl"]
  
}
