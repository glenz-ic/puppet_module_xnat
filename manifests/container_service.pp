# == Class: xnat::container_service
#
# Helper class to install Container Service plugin, and optional local Docker environment.
# 
# === Parameters
#
# Below are parameters expected by this class. Required vs. optional parameters are noted.
#
# [*plugin_url*]
#   URL for container service plugin jar
#   Default: https://github.com/NrgXnat/container-service/releases/download/1.0-alpha.1/containers-all-1.0-alpha.1-all.jar
#
# [*install_docker*]
#   Whether to install local Docker (via garethr/docker module).
#   Default: true
#
# [*docker_users*]
#   Array of users to add to the docker group.  The user $::xnat::tomcat_user will be added to the
#   array if $::xnat::manage_tomcat_user = true, so don't specify that user here.
#
# === Variables
#
# Custom variables used by this class.
#
# NONE
#
# === Examples
#
#  class { 'xnat::container_service':
#    install_docker => false,
#  }
#
# === Authors
#
# NRG <webmaster@nrg.wustl.edu>
#
# === Copyright
#
# Copyright 2016 NRG.
#
class xnat::container_service (
  String $plugin_url = 'https://github.com/NrgXnat/container-service/releases/download/1.0-alpha.1/containers-all-1.0-alpha.1-all.jar',
  Boolean $install_docker = true,
  Optional[Array] $docker_users = undef,
  ) {

  include wget
  include xnat

  # Only supported under XNAT v1.7
  if $::xnat::version != '1.7' {
    file ( "XNAT Container Service not supported for version ${::xnat::version}.")
  }

  if $install_docker {
    # Add XNAT/Tomcat user to docker group
    if $::xnat::tomcat_manage_user and $docker_users {
      $docker_users_real = concat($docker_users, $::xnat::tomcat_user)
    }
    elsif $docker_users {
      $docker_users_real = $docker_users
    }
    
    # Additional params can be passed to docker module via hiera
    include docker
    Class['xnat::tomcat'] -> Class['docker']

    if $docker_users_real {
      docker::system_user { $docker_users_real:
        create_user => false,
        require => Class['docker'],
      }
    }
  }

  # Retrieve CS plugin jar from URL specified, unless already downloaded
  $fetch_dest = "${::xnat::data_root}/src/"
  $plugin_filename = basename($plugin_url)
  wget::fetch { 'cs_plugin':
    source => $plugin_url,
    destination => $fetch_dest,
    execuser => $::xnat::tomcat_user,
    unless => "test -f ${fetch_dest}${plugin_filename}",
    require => File["${::xnat::data_root}/src/"],
    notify => Exec['deploy_cs_plugin'];
  }
  
  # Deploy the CS plugin
  exec {
    'deploy_cs_plugin':
      command => "cp ${::xnat::data_root}/src/${plugin_filename} ${::xnat::xnat_home_real}/plugins",
      path    => ['/usr/bin', '/usr/sbin', '/bin'],
      user => $::xnat::tomcat_user,
      refreshonly => true,
      require => File["${::xnat::xnat_home_real}/plugins"],
      notify => [ Exec['xnat_deploy_war'], Tomcat::Service["${::xnat::project_name}"] ],
  }
}
