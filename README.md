# xnat

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with xnat](#setup)
    * [What xnat affects](#what-xnat-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with xnat](#beginning-with-xnat)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

This is the base Puppet module for deploying a basic instance of XNAT.

## Module Description

This module will deploy a production instance of XNAT, along with a Tomcat container and, if
specified, an Apache front-end.  The XNAT deployment expects a PostgreSQL host + database, which
this module does not deploy.  Likewise, this module does not deploy any SSL cert files.  This
module thus far only tested for the following platforms:

* CentOS/RHEL 6 with OpenJDK 7
* CentOS/RHEL 7 with OpenJDK 7
* CentOS/RHEL 7 with OpenJDK 8

Also refer to example manifests in [examples](examples).

## Setup

### What xnat affects

* XNAT deployment and configuration, whether from source or WAR
* Tomcat installation and configuration
* Apache front-end and SSL configuration, if enabled

### Setup Requirements

Check puppet version matches the one required in [metadata.json](metadata.json)

Put the XNAT module (this repo) in your puppet modules folder. Caveat: Your module folder name must match the class name in the script you are running. In order to run deploy_xnat17.pp you'd need to put the contents of this repo into something like /etc/puppetlabs/code/environments/production/modules/xnat

The XNAT module requires the some 3rd party modules, specified in [metadata.json](metadata.json).

To install the stldlib module, for example, run the following on your Puppet master:

~~~
puppet module install puppetlabs-stdlib
~~~

You could also give [librarian-puppet](http://librarian-puppet.com/) a try to manage dependencies. 

### Beginning with xnat


The simplest way to get XNAT up and running is to install Java, a local PostgreSQL server/role/DB, and then declare the XNAT module.

Example manifests that do this are listed in [examples](examples).

In case you want to run a puppet script locally in order to test something, just use `puppet apply <script.pp>`

## Usage

MORE TBD

Put the classes, types, and resources for customizing, configuring, and doing
the fancy stuff with your module here.

## Reference

MORE TDB

Here, list the classes, types, providers, facts, etc contained in your module.
This section should include all of the under-the-hood workings of your module so
people know what the module is touching on their system but don't need to mess
with things. (We are working on automating this section!)

## Limitations

MORE TDB

This is where you list OS compatibility, version compatibility, etc.

Currently limited functionality on Debian based systems, however it is being worked on.

## Development

MORE TDB

Since your module is awesome, other users will want to play with it. Let them
know what the ground rules for contributing are.

TODO: Maybe use flyway or liquibase if puppet has to manage DB schema updates
https://forge.puppetlabs.com/cpitman/database_schema

## Release Notes/Contributors/Etc **Optional**

MORE TDB

If you aren't using changelog, put your release notes here (though you should
consider using changelog). You may also add any additional sections you feel are
necessary or important to include here. Please use the `## ` header.
